package controller

import com.falabella.codechallange.product.ProductApplication
import org.springframework.test.context.ContextConfiguration
import org.springframework.test.context.web.WebAppConfiguration
import spock.lang.Specification

@WebAppConfiguration
@ContextConfiguration(classes = ProductApplication)
class ProductApplicationSpec extends Specification {

    def "context should load"() {
        expect:
        true
    }

    def "main"(){
        given:
            ProductApplication.main(new String[] {});
        expect:
            true
    }

}

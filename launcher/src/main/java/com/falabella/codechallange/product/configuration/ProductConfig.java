package com.falabella.codechallange.product.configuration;

import com.falabella.codechallange.product.adapters.ProductJpaAdapter;
import com.falabella.codechallange.product.ports.api.ProductServicePort;
import com.falabella.codechallange.product.ports.spi.ProductPersistencePort;
import com.falabella.codechallange.product.repository.ProductRepository;
import com.falabella.codechallange.product.service.ProductServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ProductConfig {
    @Autowired
    ProductRepository productRepository;

    @Bean
    public ProductPersistencePort productPersistence(){
        return new ProductJpaAdapter(productRepository);
    }

    @Bean
    public ProductServicePort productService(){
        return new ProductServiceImpl(productPersistence());
    }
}

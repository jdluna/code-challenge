## Code Challenge

Thisrepo shows a proposal for the code challenge of the creation of an REST API for a product, based on the use of an Hexagonal Architecture

A definition for the proposal architecture which contains following structure:

```shell
+-- domain:  # core of our application. Contains pure domain business logic, doesn't have any external dependencies. All communications goes through ports.
|   +-- data # Core object model of the Domain
|   +-- ports # To allow the outside to interact with the domain, the hexagon provides business interfaces divided into two categories.
|       +-- api # gathers all the interfaces for everything that needs to query the domain.
|       +-- spi # gathers all the interfaces required by the domain to retrieve information from third parties.
|   +-- service # Domain services.
+-- infraestructure # connects to RDBMS-s, NoSQL DBs, Object Storages, etc. In this case H2
+-- application # Service Provider Interface - contains secondary/driven adapters.
+-- launcher # This module contains the application which will instantiate any of the previously highlighted modules so it runs a stand-alone application with a specific configuration..
```
*** *One of many ways to structure the packages, always discuss and agree inside the team :D*

Follow diagram is a reference of the components involved: 

![alt text](img/code-challenge.png "Arquitectura Propuesta")

Improvements can be made to the proposal, it is well structured and changes can be done from within any component mentioned without refering to another, it is an important advantage of this design.

## Getting Started

### Requirements

* Java 11
* Gradle 6.* and above
* Docker is optional, in case you have installed and want to test it, check option for it

### Building and running the application locally
#### Clone the project

```bash
# HTTPS
git clone https://gitlab.com/jdluna/code-challenge.git

# or

# SSH
git clone git@gitlab.com:jdluna/code-challenge.git
```

#### The actual build
In order to build the project is necessary to be at the root of the directory for the project after cloning and then execute:
```
./gradle build
```
To run application locally:
```
./gradle :launcher:bootRun
```
Endpoints will be accessible at:
```
http://localhost:8080/
```

#### Important Endpoints

|                                Name |                 Endpoint                  | 
|------------------------------------:|:-----------------------------------------:|
|          `Product Service - Create` |     http://localhost:8080/product/add     |
|         `Product Service - Get All` |     http://localhost:8080/product/get     |
|       `Product Service - Get By Id` |  http://localhost:8080/product/get/{id}   |
|          `Product Service - Update` |   http://localhost:8080/product/update    |
|    `Product Service - Delete By Id` | http://localhost:8080/product/delete/{id} |
|   `Product Service - Open API docs` |       http://localhost:8080/v1/api        |
|      `Product Service - Swagger UI` | http://localhost:8080/documentation.html  |
|  `Product Service - H2 console (*)` |     http://localhost:8080/h2-console      |

*** *By default the console is off. If need it you can add the following code to the application.yaml
```
spring:
  h2:
    console.enabled: true
```
### Building and running the application using docker

#### Clone the project

```bash
# HTTPS
git clone https://gitlab.com/jdluna/code-challenge.git

# or

# SSH
git clone git@gitlab.com:jdluna/code-challenge.git
```

#### The actual docker commands
In order to build the docker image and create the container:
```
docker build -t challenge .
```
To run the container:
```
docker run -d -p 8080:8080 challenge
```
Endpoints will be accessible at:
```
http://localhost:8080/
```

### Jacoco code Coverage

An integration to jacoco library was made on the project, to check results you can run:

```
./gradle codeCoverageReport

```
The results of the report are in file:
```
build/reports/jacoco/codeCoverageReport/html/index.html
```

### Integration with IntelliJ IDEA

After cloning the repo, you have the option to import the project into IntelliJ Idea IDE using option:
```
File > New > Project from Existing Sources > {Choose directory and Maven as Build Tool}
```

## Notes

For this challenge, many fast way options were taken:

* Regarding the use of the H2 databases, for practical reasons and for fast manual testing
* Even though a 85% coverage on tests was demand it on the challenge, due to time such a requirement was not met
* Integration with Jacoco and a consolidated report was done
* A Dockerfile is created thinking in implementation of a ci/cd process
* Gitlab as a source code repo was chosen for that capability, but no configuration was made on site
* Moving to a hexagonal architecture was a chooise that merge the different experiences I had, regarding microservices architecture, distributed system. That basis is set here, but a more robust implementation can be made.
* Healthchecks were enabled in case need it for a kubernetes cluster deployment.
* Metrics are important and it is something that in the implementation is not done, but Micrometer and Datadog should be the option of choice to recollect metrics.

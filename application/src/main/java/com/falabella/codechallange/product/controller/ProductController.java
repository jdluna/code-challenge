package com.falabella.codechallange.product.controller;

import com.falabella.codechallange.product.data.ProductDto;
import com.falabella.codechallange.product.ports.api.ProductServicePort;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/product")
@RequiredArgsConstructor
public class ProductController {

    private final ProductServicePort productServicePort;

    @PostMapping("/add")
    public ResponseEntity<ProductDto>  addProduct(@Valid @RequestBody ProductDto productDto) {
        log.info("{}", productDto);
        return new ResponseEntity<>(productServicePort.addProduct(productDto), HttpStatus.CREATED);
    }

    @PutMapping("/update")
    public ResponseEntity<ProductDto>  updateProduct(@RequestBody ProductDto productDto) {
        return new ResponseEntity<>(productServicePort.updateProduct(productDto), HttpStatus.OK);
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<ProductDto> getProductById(@PathVariable Long id) {
        if(productServicePort.getProductById(id).isPresent()){
            return new ResponseEntity<>(productServicePort.getProductById(id).get(), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping("/get")
    public ResponseEntity<List<ProductDto>> getAllProducts() {
        return new ResponseEntity<>(productServicePort.getProducts(), HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<Void> deleteProductById(@PathVariable Long id) {
        productServicePort.deleteProductById(id);
        return ResponseEntity.noContent().build();
    }

}

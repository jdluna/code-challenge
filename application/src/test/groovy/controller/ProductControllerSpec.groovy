package controller

import com.falabella.codechallange.product.controller.ProductController
import com.falabella.codechallange.product.data.ProductDto
import com.falabella.codechallange.product.ports.api.ProductServicePort
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import org.spockframework.spring.SpringBean
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders as ReqBuilder
import org.springframework.test.web.servlet.setup.MockMvcBuilders
import spock.lang.Specification

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@AutoConfigureMockMvc
class ProductControllerSpec extends Specification{
    @SpringBean
    ProductServicePort productServicePort = Mock()
    ProductController productController
    MockMvc mockMvc
    ObjectMapper mapper

    def setup(){
        productController = new ProductController(productServicePort)
        mockMvc = MockMvcBuilders.standaloneSetup(productController).build()
        mapper = new ObjectMapper()
    }

    def "get by product Id"(){
        given:
            def productDto = new ProductDto(id:1, sku: 'FAL-8406270', name: 'Urbana Mujer',
                    brand: 'New Balance', size: '37', price: 42990.00, principalImage: 'https://falabella.scene7.com/is/image/Falabella/8406270_1')
            productServicePort.getProductById(1) >> Optional.of(productDto)

        when:
            def response = mockMvc.perform(ReqBuilder.get("/product/get/1").contentType(MediaType.APPLICATION_JSON))
            def result = mapper.readValue(response.andReturn().getResponse().getContentAsString(), new TypeReference<ProductDto>(){})
        then:
            response.andExpect(status().isOk())
            result == productDto
    }

    def "get by product Id not found"(){
        given:
            productServicePort.getProductById(1) >> Optional.empty()
        when:
            def response = mockMvc.perform(ReqBuilder.get("/product/get/1").contentType(MediaType.APPLICATION_JSON))
        then:
            response.andExpect(status().isNotFound())
    }

    def "get All products"(){
        given:
            def productDto1 = new ProductDto(id:1, sku: 'FAL-8406270', name: 'Urbana Mujer',
                brand: 'New Balance', size: '37', price: 42990.00, principalImage: 'https://falabella.scene7.com/is/image/Falabella/8406270_1')
            def productDto2 = new ProductDto(id:2, sku: 'FAL-881952283', name: 'Bicicleta Baltoro Aro 29',
                brand: 'Jeep', size: 'ST', price: 399990.00, principalImage: 'https://falabella.scene7.com/is/image/Falabella/8406270_1', pathImageList: ['https://falabella.scene7.com/is/image/Falabella/881952283_2'])
            productServicePort.getProducts() >> [productDto1, productDto2].asList()
        when:
            def response = mockMvc.perform(ReqBuilder.get("/product/get").contentType(MediaType.APPLICATION_JSON))
            def result = mapper.readValue(response.andReturn().getResponse().getContentAsString(), new TypeReference<List<ProductDto>>(){})
        then:
            response.andExpect(status().isOk())
            result.size() == 2
            result.get(0).id == productDto1.id
    }

    def "delete by Id"(){
        given:
            productServicePort.deleteProductById(1) >> {}
        when:
            def response = mockMvc.perform(ReqBuilder.delete("/product/delete/1").contentType(MediaType.APPLICATION_JSON))
        then:
            response.andExpect(status().isNoContent())
    }

    def "Add product"(){
        given:
            def productDto = new ProductDto(sku: 'FAL-8406270', name: 'Urbana Mujer',
                brand: 'New Balance', size: '37', price: 42990.00, principalImage: 'https://falabella.scene7.com/is/image/Falabella/8406270_1')
            productServicePort.addProduct(_) >> productDto
        when:
            def response = mockMvc.perform(ReqBuilder.post("/product/add")
                                                    .contentType(MediaType.APPLICATION_JSON)
                                                    .content(mapper.writeValueAsString(productDto)))
            def result = mapper.readValue(response.andReturn().getResponse().getContentAsString(), new TypeReference<ProductDto>(){})
        then:
            response.andExpect(status().isCreated())
            result.sku == productDto.sku
    }

    def "update Product"(){
        given:
            def previousProductDto = new ProductDto(id:1, sku: 'FAL-8406270', name: 'Urbana Mujer',
                brand: 'New Balance', size: '37', price: 42990.00, principalImage: 'https://falabella.scene7.com/is/image/Falabella/8406270_1')

            def productDto = new ProductDto(id:1, sku: 'FAL-8406270', name: 'Urbana Mujer',
                brand: 'New Balance 2', size: '37', price: 42990.00, principalImage: 'https://falabella.scene7.com/is/image/Falabella/8406270_1')
            productServicePort.addProduct(_) >> previousProductDto
            productServicePort.updateProduct(_) >> productDto
        when:
            def response = mockMvc.perform(ReqBuilder.post("/product/add")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(previousProductDto)))
            def result = mapper.readValue(response.andReturn().getResponse().getContentAsString(), new TypeReference<ProductDto>(){})
        then:
            response.andExpect(status().isCreated())
            result.brand == previousProductDto.brand
        when:
            response = mockMvc.perform(ReqBuilder.put("/product/update")
                .contentType(MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(productDto)))
            result = mapper.readValue(response.andReturn().getResponse().getContentAsString(), new TypeReference<ProductDto>(){})
        then:
            response.andExpect(status().isOk())
            result.brand != previousProductDto.brand
            result.brand == productDto.brand
    }
}

package com.falabella.codechallange.product.adapters;

import com.falabella.codechallange.product.data.ProductDto;
import com.falabella.codechallange.product.entity.Product;
import com.falabella.codechallange.product.mapper.ProductMapper;
import com.falabella.codechallange.product.ports.spi.ProductPersistencePort;
import com.falabella.codechallange.product.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class ProductJpaAdapter implements ProductPersistencePort {
    private final ProductRepository productRepository;

    @Override
    public ProductDto addProduct(ProductDto productDto) {
        Product product = ProductMapper.INSTANCE.productDtoToProduct(productDto);
        Product productSaved = productRepository.save(product);
        return ProductMapper.INSTANCE.productToProductDto(productSaved);
    }

    @Override
    public void deleteProductById(Long id) {
        productRepository.deleteById(id);
    }

    @Override
    public ProductDto updateProduct(ProductDto productDto) {
        return addProduct(productDto);
    }

    @Override
    public List<ProductDto> getProducts() {
        List<Product> productList = productRepository.findAll();

        return ProductMapper.INSTANCE.productListToProductDtoList(productList);
    }

    @Override
    public Optional<ProductDto> getProductById(Long productId) {
        Optional<Product> product = productRepository.findById(productId);

        if (product.isPresent()) {
            return Optional.of(ProductMapper.INSTANCE.productToProductDto(product.get()));
        }
        return Optional.empty();
    }
}

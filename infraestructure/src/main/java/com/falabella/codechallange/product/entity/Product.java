package com.falabella.codechallange.product.entity;

import com.falabella.codechallange.product.converter.ListToStringConverter;
import com.sun.istack.NotNull;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;


@Getter
@Setter
@Data
@Entity
public class Product {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    @Column(name="product_id")
    private Long id;

    @NotNull
    @Column(name="product_sku", nullable = false)
    private String sku;

    @NotNull
    @Column(name="product_name", nullable = false)
    private String name;

    @NotNull
    @Column(name="product_brand", nullable = false)
    private String brand;

    @Column(name="product_size")
    private String size;

    @NotNull
    @Column(name = "product_price", precision = 10, scale = 2, nullable = false)
    private BigDecimal price;

    @NotNull
    @Column(name = "product_principal_image", nullable = false)
    private String principalImage;

    @Column(name = "product_images")
    @Convert(converter = ListToStringConverter.class)
    private List<String> pathImageList;
}

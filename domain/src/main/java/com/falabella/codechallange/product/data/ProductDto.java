package com.falabella.codechallange.product.data;

import lombok.*;
import org.hibernate.validator.constraints.URL;

import javax.validation.constraints.*;
import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ProductDto {
    private Long id;
    @NotEmpty(message = "The sku is required.")
    @Pattern(regexp="(?i)FAL-(\\d{7}|\\d{8})", message = "Must be in format FAL-####### or FAL-########")
    private String sku;
    @NotEmpty(message = "The name is required.")
    private String name;
    @NotEmpty(message = "The brand is required.")
    private String brand;
    private String size;
    @NotNull(message = "The sell price is required.")
    @DecimalMin(value = "1.00", message = "The sell price cam not ne less than 1.00")
    @DecimalMax(value = "99999999.00", message = "The sell price can not be more than 99999999.00")
    @Digits(integer = 8, fraction = 2)
    @Positive(message = "The sell price can not be negative")
    private BigDecimal price;
    @NotEmpty(message = "The principal image url path is required.")
    @URL(message = "The principal image url must be a valid url address")
    private String principalImage;
    private List<String> pathImageList;
}

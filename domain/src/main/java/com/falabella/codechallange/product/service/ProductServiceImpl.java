package com.falabella.codechallange.product.service;

import com.falabella.codechallange.product.data.ProductDto;
import com.falabella.codechallange.product.ports.api.ProductServicePort;
import com.falabella.codechallange.product.ports.spi.ProductPersistencePort;
import lombok.RequiredArgsConstructor;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public class ProductServiceImpl implements ProductServicePort {
    private final ProductPersistencePort productPersistencePort;

    @Override
    public ProductDto addProduct(ProductDto productDto) {
        return productPersistencePort.addProduct(productDto);
    }

    @Override
    public void deleteProductById(Long id) {
        productPersistencePort.deleteProductById(id);
    }

    @Override
    public ProductDto updateProduct(ProductDto productDto) {
        return productPersistencePort.updateProduct(productDto);
    }

    @Override
    public List<ProductDto> getProducts() {
        return productPersistencePort.getProducts();
    }

    @Override
    public Optional<ProductDto> getProductById(Long productId) {
        return productPersistencePort.getProductById(productId);
    }
}

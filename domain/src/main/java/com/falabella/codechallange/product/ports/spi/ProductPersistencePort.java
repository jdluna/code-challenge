package com.falabella.codechallange.product.ports.spi;

import com.falabella.codechallange.product.data.ProductDto;

import java.util.List;
import java.util.Optional;

public interface ProductPersistencePort {
    ProductDto addProduct(ProductDto productDto);

    void deleteProductById(Long id);

    ProductDto updateProduct(ProductDto productDto);

    List<ProductDto> getProducts();

    Optional<ProductDto> getProductById(Long productId);
}

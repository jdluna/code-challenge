package com.falabella.codechallange.product.port.api

import com.falabella.codechallange.product.data.ProductDto
import com.falabella.codechallange.product.ports.api.ProductServicePort
import com.falabella.codechallange.product.ports.spi.ProductPersistencePort
import com.falabella.codechallange.product.service.ProductServiceImpl
import spock.lang.Specification

class ProductServicePortSpec extends Specification {
    ProductServicePort productServicePort
    ProductPersistencePort productPersistencePort

    def setup(){
        productPersistencePort = Mock(ProductPersistencePort)
        productServicePort = new ProductServiceImpl(productPersistencePort)
    }

    def "get product by id"(){
        given:
            def productDto = new ProductDto(id:1, sku: 'FAL-8406270', name: 'Urbana Mujer',
                brand: 'New Balance', size: '37', price: 42990.00, principalImage: 'https://falabella.scene7.com/is/image/Falabella/8406270_1')
            productPersistencePort.getProductById(productDto.id) >> Optional.of(productDto)
        when:
            def result = productServicePort.getProductById(productDto.id)
        then:
            result.get() == productDto
    }

    def "fail when product not foud by id"(){
        given:
            productPersistencePort.getProductById(_) >> Optional.empty()
        when:
            def result = productServicePort.getProductById(1)
        then:
            result == Optional.empty()
    }

    def "get all products"(){
        given:
            def productDto1 = new ProductDto(id:1, sku: 'FAL-8406270', name: 'Urbana Mujer',
                brand: 'New Balance', size: '37', price: 42990.00, principalImage: 'https://falabella.scene7.com/is/image/Falabella/8406270_1')
            def productDto2 = new ProductDto(id:2, sku: 'FAL-881952283', name: 'Bicicleta Baltoro Aro 29',
                brand: 'Jeep', size: 'ST', price: 399990.00, principalImage: 'https://falabella.scene7.com/is/image/Falabella/8406270_1', pathImageList: ['https://falabella.scene7.com/is/image/Falabella/881952283_2'])
            productPersistencePort.getProducts() >> [productDto1, productDto2].asList()
        when:
            def result = productServicePort.getProducts()
        then:
            result.size() == 2
            result.get(0) == productDto1
    }

    def "delete product by product id"(){
        given:
            productPersistencePort.deleteProductById(1) >> {}
            productPersistencePort.getProductById(1) >> Optional.empty()
        when:
            def result = productServicePort.deleteProductById(1)
            def resultado = productServicePort.getProductById(1)
        then:
            resultado == Optional.empty()
    }

    def "update product by product Id"(){
        given:
            def previousProductDto = new ProductDto(id:1, sku: 'FAL-8406270', name: 'Urbana Mujer',
                brand: 'New Balance', size: '37', price: 42990.00, principalImage: 'https://falabella.scene7.com/is/image/Falabella/8406270_1')

            def productDto = new ProductDto(id:1, sku: 'FAL-8406270', name: 'Urbana Mujer',
                brand: 'New Balance 2', size: '37', price: 42990.00, principalImage: 'https://falabella.scene7.com/is/image/Falabella/8406270_1')
        when:
            def result = productServicePort.addProduct(previousProductDto)
        then:
            1 * productPersistencePort.addProduct(previousProductDto)
        when:
            productPersistencePort.updateProduct(_) >> productDto
            def resultado = productServicePort.updateProduct(productDto)
        then:
            resultado.brand != previousProductDto.brand
            resultado.brand == productDto.brand

    }

    def "returns proper value at insert"(){
        given:
            def productDto = new ProductDto(sku: 'FAL-8406270', name: 'Urbana Mujer',
                brand: 'New Balance', size: '37', price: 42990.00, principalImage: 'https://falabella.scene7.com/is/image/Falabella/8406270_1')
        when:
            def result = productServicePort.addProduct(productDto)
        then:
            1 * productPersistencePort.addProduct(productDto)
    }
}
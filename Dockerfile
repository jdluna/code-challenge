FROM openjdk:11 AS BUILD_IMAGE
ENV APP_HOME=/root/dev/myapp
#RUN mkdir -p $APP_HOME/src/main/java
WORKDIR $APP_HOME
COPY . .
COPY build.gradle gradlew gradlew.bat $APP_HOME
COPY gradle $APP_HOME/gradle
# download dependencies
RUN ./gradlew build -x :launcher:bootJar -x test --continue

RUN ./gradlew build

FROM openjdk:11-jre
WORKDIR /root/
COPY --from=BUILD_IMAGE /root/dev/myapp/launcher/build/libs/launcher-0.0.1-SNAPSHOT.jar /root/myapp.jar
EXPOSE 8080
CMD ["java","-jar","myapp.jar"]